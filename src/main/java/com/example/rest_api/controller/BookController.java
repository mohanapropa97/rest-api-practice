package com.example.rest_api.controller;
import com.example.rest_api.entity.Book;
import com.example.rest_api.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    public BookService bookService;

    @GetMapping(value = "/books")
    //@ResponseBody
    public List<Book> getBooks() {
        return bookService.getBooks();
    }

    @GetMapping(value = "/books/{id}")
    public Book getBookByID(@PathVariable("id") int id) {

        return bookService.getBookByID(id);

    }
@PostMapping( "/books")
    public Book addBook(@RequestBody Book book)
    {
        Book b = this.bookService.addBook(book);
        return b;
    }

    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable("id") int id)
    {
        bookService.deleteBook(id);
        //return book;
    }

    @PutMapping("/books/{id}")
    public Book updateBook(@RequestBody Book book, @PathVariable("id") int id)
    {
        bookService.updateBook(book, id);
        return book;
    }


}
