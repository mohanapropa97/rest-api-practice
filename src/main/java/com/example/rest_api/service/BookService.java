package com.example.rest_api.service;

import com.example.rest_api.entity.Book;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookService {
    private static List<Book> bookList = new ArrayList<>();

    static {
        bookList.add(new Book(101, "aaa", "XXX"));
        bookList.add(new Book(102, "aaa", "XXX"));
        bookList.add(new Book(103, "aaa", "XXX"));
    }

    public List<Book> getBooks() {
        return bookList;
    }

    public Book getBookByID(int id) {

        Book book = bookList.stream().filter(e -> e.getId() == id).findFirst().get();
        return book;

    }

    public Book addBook(Book book)
    {
        bookList.add(book);
        return book;
    }

    public void deleteBook(int bid)
    {
         bookList= bookList.stream().filter(book ->book.getId()!= bid).collect(Collectors.toList());

    }

    public void updateBook(Book book, int id)
    {
        bookList.stream().map(b->{

            if(b.getId() == id)
            {
                b.setAuthor(book.getAuthor());
                b.setBookName(book.getBookName());
            }
            return b;
        } ).collect(Collectors.toList());
    }


}
